# 12 Principles of Animation done as CSS Animations

This is an exercise in building 12 simple examples that illustrate the 12 Principles of Animation from the "Illusion Of Life" by Frank Thomas & Ollie Johnston, Disney animators from the 1930's.

The Principles are:

1. Squash and Stretch
2. Anticipation
3. Staging
4. Straight Ahead Action and Pose to Pose
5. Follow Through and Overlapping Action
6. Slow In and Slow Out
7. Arc
8. Secondary Action
9. Timing
10. Exaggeration
11. Solid drawing

Eventually this project should include JavaScript, canvas and GSAP animations.
